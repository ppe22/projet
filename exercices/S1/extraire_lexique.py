#!/usr/bin/python3
# -*- coding: utf-8 -*-

#
# Modules to import
#
from collections import defaultdict
import os
import sys
import numpy as np
import pandas as pd
import argparse
import thulac
import errno
import sys

#
# User functions
#


# r1
# path = /path/to/projet/exercice/S1/Corpus
def string_list(filePath):
    """
    construit une liste de chaînes de caractères
    où chaque chaîne correspondra au contenu 
    texte d'un fichier
    """
    l = []
    seg = thulac.thulac(seg_only=True)
    
    # open files and increment list
    for file in filePath:
        if os.path.isfile(file):
            with open(file, 'r') as text:
                raw = text.read()
                # Prétraitement du texte
                ## suppresison des retours à la ligne
                raw = raw.replace("\n", "")
                try:
                    # Segmentation du chinois
                    l.append(seg.cut(raw, text=True))
                except IOerror as e:
                    if e.errno != errno.EPIPE:
                        raise
                else:
                    l.append(raw)
    return l

# r2
def compte_mots(liste_chaines):
	""" Une fonction qui prend comme argument une liste de chaines et retourne un dictionnaire avec le decompte de chaque mot """
	res = defaultdict(lambda: 0)
	for chaine in liste_chaines:
		for word in chaine.split(' '):
			res[word] += 1
	return res

#r3
def wordDicDoc(texts):
    """
    texts : liste des documents
    texts contient des chaînes de caractères

    retourne dictionnaire associant chaque mot au nombre de 
    documents dans lequel il apparaît
    """
    dico = {}
    for text in texts: # 0, 1
        d = {}
        dump = text.strip().split() # liste des mots du texte
        countWord = pd.value_counts(np.array(dump)) # mots uniques et occurrences
        words = countWord.index # liste mots uniques
        # remplir dictionnaire
        d = dict(zip(words, np.ones(len(words), dtype=int)))
        dico.update(d)
    return dico

# Semaine 3
# appel des fonctions ci-dessus
def main():
    if len(sys.argv) > 1:
        # Parseur qui lit les fichiers passés en argument
        # et les enregistre dans une liste args.files :
        # (r1 semaine 3)
        parser = argparse.ArgumentParser()
        parser.add_argument('files', type=str, nargs='+')
        args = parser.parse_args()
        fichier = args.files

	# Test des fonctions 
        texts = string_list(fichier)
        mots_comptes = compte_mots(texts)
        DicDoc = wordDicDoc(texts)

        # récupérer les arguments de ligne de commande
        #fichier = sys.argv[1:]
    else:
        # r3
    	# lister les fichiers du corpus sur l’entrée standard
        data = sys.stdin.read()
        fichier = data.splitlines()
        if len(fichier) > 0 and os.path.isfile(fichier[0]):
        # parcourir la liste de fichiers et afficher le nom de chaque fichier"。
            for file in fichier:
                print(f'{file}')#
        else:
            # r2
            textStripped = []
            for e in data:
                textStripped.append(e.strip("\n"))

            # test des fonctions
            mots_comptes = compte_mots(textStripped)
            print(mots_comptes)
            dicDoc = wordDicDoc(textStripped)
            print(dicDoc)

#
# Main function
#
if __name__ == "__main__":
	main()
