#!/bin/python3

import spacy
from spacy_lefff import LefffLemmatizer, POSTagger
from spacy.language import Language
#import stanza

from datastructures import POS, Token


import nltk
from nltk import word_tokenize
from nltk.tokenize import sent_tokenize
from nltk.stem import WordNetLemmatizer

#
# Global var
#
@Language.factory('french_lemmatizer')
def create_french_lemmatizer(nlp, name):
	return LefffLemmatizer()

# Chargement de spacy
__nlp = spacy.load('fr_core_news_sm')
__nlp.add_pipe('french_lemmatizer', name='lefff')

# Chargement de stanza
#stanza.download('fr')
#__nlp_stanza = stanza.Pipeline('fr')

	
#
# User functions
#
def analyze(text, method="spacy"):
	if method == "spacy":
		return analyze_spacy(text)
	elif method == "stanza":
		return analyze_stanza(text)
	elif method == "nltk":
		return analyze_nltk(text)

def analyze_spacy(text):
	doc = __nlp(text)
	for mot in doc:
		token = Token(mot.text, str(mot._.lefff_lemma), POS[mot.tag_])  #mot.lemma_, POS[mot.pos_])
		yield token

def analyze_stanza(text):
    doc = __nlp_stanza(text)
    for sent in doc.sentences:
        for word in sent.words:
            token = Token(word.text, word.lemma, POS[word.upos])
            yield token
		
def analyse_nltk(text):
	tokens = word_tokenize(text, language='french')
	pos_tags = nltk.pos_tag(tokens)
	# lemmatizer init
	lemmatizer = WordNetLemmatizer()
	
	for t in tokens:
		index = tokens.index(t)
		token = Token(t, lemmatizer.lemmatize(t), pos_tags[index][1]) # form, lemma, pos_tag
		yield token
