#!/bin/python3

import io
import os.path
import re
import tarfile
import argparse

import smart_open
from pprint import pprint

from gensim.models import Phrases
from gensim.corpora import Dictionary
from gensim.models import LdaModel
import pyLDAvis
import pyLDAvis.gensim_models as gensimvis

from data_format import format_output
from datastructures import POS
from load_corpus import fichier_to_corpus
from pathlib import Path


def load_sous_corpus(fichier: Path, format_in: format_output):
	corpus = fichier_to_corpus(fichier, format_in)
	return corpus

def add_bigrams(docs, min_count=20):
	bigram = Phrases(docs, min_count=5)
	for idx in range(len(docs)):
		for token in bigram[docs[idx]]:
			if '_' in token:
			# Token is a bigram, add to document.
				docs[idx].append(token)
	return docs


def build_lda_model(
		docs,
		num_topics = 10,
		chunksize = 2000,
		passes = 20,
		iterations = 400,
		eval_every = None,
		no_below=10, # les mots doivent apparaitre + de 20 fois
		no_above=0.5
		):


	dictionary = Dictionary(docs)
	# Filter out words that occur less than 20 documents, or more than 50% of the documents.
	# Filter out words that occur less than 10% of the documents, or more than 50% of the documents.
	dictionary.filter_extremes(no_below=no_below, no_above=no_above)
	corpus = [dictionary.doc2bow(doc) for doc in docs]
	print('Number of unique tokens: %d' % len(dictionary))#,sys.stderr)
	print('Number of documents: %d' % len(corpus))

	temp = dictionary[0]  # This is only to "load" the dictionary.
	id2word = dictionary.id2token

	model = LdaModel(
		corpus=corpus,
		id2word=id2word,
		chunksize=chunksize,
		alpha='auto',
		eta='auto',
		iterations=iterations,
		num_topics=num_topics,
		passes=passes,
		eval_every=eval_every)
	return corpus, dictionary, model

def print_coherence(model, corpus):
	top_topics = model.top_topics(corpus)

# Average topic coherence is the sum of topic coherences of all topics, divided by the number of topics.
	avg_topic_coherence = sum([t[1] for t in top_topics]) / model.num_topics
	print('Average topic coherence: %.4f.' % avg_topic_coherence)

	from pprint import pprint
	pprint(top_topics)


def save_html_viz(model, corpus, dictionary, output_path):
	"""
	model: 
	"""
	vis_data = gensimvis.prepare(model, corpus, dictionary)
	with open(output_path, "w") as f:
		pyLDAvis.save_html(vis_data, f)

def main(args):
	corpus = load_sous_corpus(args.fichier, args.format)
	docs = []
	filter_in = [POS.NOUN, POS.PROPN] 
	stopwords = ["«", "»", "…", "d’", "n’", "tribune_", "«tribune", "L’", "«_Monde", "Monde", "l’", "-",  "Jean-Michel", "Bezat", "Laurence", "Girard", "Philippe", "Escande", "éditorialiste", "chronique", "tribune", "journaliste"]

	if args.lemmatize:
		for article in corpus.articles:
			doc = []
			for token in article.analyse:
				if token.pos in filter_in and token.form not in stopwords:
					doc.append(token.lemma)
			if len(doc) > 0:
				docs.append(doc)
	else:
		for article in corpus.articles:
			doc = []
			for token in article.analyse:
				if token.pos in filter_in and token.form not in stopwords:
					doc.append(token.form)#, token.lemma, token.pos.value])
			# On peut aussi générer un token avec plus d'informations en concaténant.
			#doc.append([token.form + "/" + token.pos.value + "/" + token.lemma])
			if len(doc) > 0:
				docs.append(doc)
	# Compute bigrams.
	# Add bigrams and trigrams to docs (only ones that appear 2 times or more).
	docs = add_bigrams(docs)

	# Create a dictionary representation of the documents.
	dictionary = Dictionary(docs)

	# On le ne fait plus car on ne récupère que les noms communs et propres ?


	# Finally, we transform the documents to a vectorized form. We simply compute
	# the frequency of each word, including the bigrams.
	#

	# Bag-of-words representation of the documents.
	corpus = [dictionary.doc2bow(doc) for doc in docs]

	###############################################################################
	# Let's see how many tokens and documents we have to train on.
	#

	print('Number of unique tokens: %d' % len(dictionary))
	print('Number of documents: %d' % len(corpus))


	# Train LDA model.
	# Set training parameters.
	corpus, dictionary, model = build_lda_model(docs, num_topics = args.n_topics)

	# Average topic coherence is the sum of topic coherences of all topics, divided by the number of topics.
	print_coherence(model, corpus)

	top_topics = model.top_topics(corpus)

	pprint(top_topics)

	### Enregistrement du fichier html
	save_html_viz(model, corpus, dictionary, args.output_html)

#
# Main function
#

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('-f', '--fichier', type=str, required=True)
	parser.add_argument('-F', '--format', type=format_output, required=True)
	parser.add_argument('-t', '--n_topics', type=int, default=10)
	parser.add_argument('-l', '--lemmatize', action='store_true')
	parser.add_argument('-o', '--output_html', type=str, default="output.html")
	args = parser.parse_args()
	main(args)
