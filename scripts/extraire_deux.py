#!/bin/python3
#-*- coding: utf-8 -*-

#
# Modules to import
#
import argparse
from extraire_un import extraire
import os
import glob

import data_format
from data_format import format_mois, format_categorie, format_date, path_to_date, format_extraction_method, format_analyse_method, format_output
from datastructures import Corpus
from sortie import output_corpus

#
# User functions
#
def extraire_all(fichiers, categories, begin, end, chemin, extraction_method, analyse_method):
	corpus = Corpus(categories, begin, end, chemin, [])
	for fichier in fichiers:
		article = list(extraire(fichier, extraction_method, analyse_method))
		corpus.articles.extend(article)
	return corpus

def sous_corpus_mois(mois):
	for m in mois:
		dossier = "../corpus/2022/" + m + "/*/*/*.xml"
		fichiers = glob.glob(dossier)
		return fichiers

def sous_corpus_dates(debut, fin):
	dossier = "../corpus/*/*/*/*/*.xml"	
	fichiers = glob.glob(dossier)
	sous_corpus = set()
	for fichier in fichiers:
		if path_to_date(fichier) >= debut and path_to_date(fichier) <= fin:
			sous_corpus.add(fichier)
	return sous_corpus
	
		
def sous_corpus_categories(categories):
	for c in categories:
		dossier = "../corpus/2022/*/*/*/" + c + ".xml"
		sous_corpus = glob.glob(dossier)
		return sous_corpus

def main(args):
	# constitution du sous corpus
	fichiers = []
	fichiers_f_t = sous_corpus_dates(args.from_date, args.to_date)
	fichiers_c = sous_corpus_categories(args.categories)
	# interesection entre les deux ensembles :
	fichiers = fichiers_f_t.intersection(fichiers_c)

	# extraction des informations dans les articles
	corpus = extraire_all(fichiers, args.categories, args.from_date, args.to_date, args.output, args.extraction_method, args.analyse_method)

	# écriture des informations du sous-corpus dans un fichier unique
	output_corpus(corpus, args.output, args.format_output)

#
# Main function
#

if __name__ == "__main__":
	parser = argparse.ArgumentParser()	
	parser.add_argument('-c', '--categories', type=format_categorie, nargs='*', default=data_format.liste_categories.values())
	parser.add_argument('-o', '--output', type=str, default="sous_corpus.xml")
	parser.add_argument('-f', '--from_date', type=format_date, default=format_date("2022-01-01")) # par défaut: la date la plus tôt
	parser.add_argument('-t', '--to_date', type=format_date, default=format_date("2022-12-31")) # par défaut: la date la plus tard
	parser.add_argument('-e', '--extraction_method', type=format_extraction_method, default="et")
	parser.add_argument('-a', '--analyse_method', type=format_analyse_method, default="stanza")
	parser.add_argument('-F', '--format_output', type=format_output, default="xml")
	args = parser.parse_args()
	main(args)
