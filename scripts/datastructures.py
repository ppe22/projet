# -*- coding: utf-8 -*-

"""
Classes de données du projet:

- POS (Enum)
- Token
- Article
- Corpus

"""

#
# Modules to import
#

from dataclasses import dataclass
from dataclasses import asdict # transforme l'objet en dico
from typing import List, Dict
from pathlib import Path
from enum import Enum
import pickle
import datetime


#
# Classes
#

# On pourrait remplacer POS par une str car chaque analyseur (spacy, nltk, stanza) a un système d'annotation différent, ce qui rent cette enum inutilisable pour d'autres analyseurs que spacy.
class POS(Enum):
	ADJ = "ADJ"
	ADP = "ADP"
	ADV = "ADV"
	AUX = "AUX"
	CONJ = "CONJ"
	CCONJ = "CCONJ"
	DET = "DET"
	INTJ = "INTJ"
	NOUN = "NOUN"
	NUM = "NUM"
	PART = "PART"
	PRON = "PRON"
	PROPN = "PROPN"
	PUNCT = "PUNCT"
	SCONJ = "SCONJ"
	SYM = "SYM"
	VERB = "VERB"
	X = "X"
	SPACE = "SPACE"

@dataclass
class Token:
	form: str
	lemma: str
	pos: POS

@dataclass
class Article:
	titre: str		# titre extrait du fichier xml de l'article
	description: str	# description extraite du fichier xml de l'article
	date: datetime		# date de publication
	categorie: str		# catégorie de l'article
	chemin: Path		# chemin vers le fichier contenant l'article
	analyse: List[Token]	# analyse morpho syntaxique

@dataclass
class Corpus:
	categories: List[str]	# liste des catégories du corpus
	begin: datetime		# articles à partir cette date
	end: datetime		# articles jusqu'à cette date
	chemin: Path # mettre une valeur par défaut à ../corpus/2022
	articles: List[Article]	# liste des articles dans le corpus
