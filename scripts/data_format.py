#!/bin/python3
# -*- coding: utf-8 -*-

#
# Modules to import
#

import argparse
import datetime
from pathlib import Path

"""
Gestion du format des données
"""

#
# Global vars
#

liste_mois = ["Jan",
"Feb",
"Mar",
"Apr",
"May",
"Jun",
"Jul",
"Aug",
"Sep",
"Oct",
"Nov",
"Dec"]

liste_categories = {"une":"0,2-3208,1-0,0",
"international":"0,2-3210,1-0,0",
"europe":"0,2-3214,1-0,0",
"societe":"0,2-3224,1-0,0",
"idees":"0,2-2323,1-0,0",
"economie":"0,2-3234,1-0,0",
"actualite-medias":"0,2-3236,1-0,0",
"sport":"0,2-3242,1-0,0",
"planete":"0,2-3244,1-0,0",
"culture":"0,2-3246,1-0,0",
"livres":"0,2-3260,1-0,0",
"cinema":"0,2-3476,1-0,0",
"voyage":"0,2-3546,1-0,0",
"technologies":"0,2-651865,1-0,0",
"politique":"0,57-0,64-823353,0",
"sciences":"env_sciences"}

#
# Util functions
#

def format_mois(m):
	try:
		m = int(m)
		if m < 1 or m > 12:
			msg = "Not a valid month: " + m
			raise argparse.ArgumentTypeError(msg)
		else:
			return liste_mois[m - 1]
	except:
		if m not in liste_mois:
			msg = "Not a valid month: " + m + ". It should be a number between 1 and 12 or a month in " + ' '.join(liste_mois) + "."
			print(msg)
			raise argparse.ArgumentTypeError(msg)
		else:
			return m

def format_categorie(c):
	if c in liste_categories.keys():
		return liste_categories[c]
	elif c in liste_categories.values():
		return c
	else:
		msg = "Not a valid category: " + c + ". Possible categories are: " + liste_categories.keys()
		print(msg)
		raise argparse.ArgumentTypeError(msg)

def format_date(d):
	
	try:
		if datetime.date.fromisoformat(d):
			return datetime.datetime.strptime(d, '%Y-%m-%d').date()
		else:
			return None
	except ValueError:
		raise ValueError("Incorrect date format, should be YYYY-MM-DD")

def format_extraction_method(e):
	if e not in ["et", "re", "fp"]:
		msg = "Extraction method " + e + " unknown. It should be either et (ElementTree), re (Regular Expression) or fp (feedparser)."
		print(msg)
		raise argparse.ArgumentError(msg)
	return e

def format_analyse_method(a):
	if a not in ["spacy", "stanza"]:
		msg = "Analyse method " + a + " unknown. It should be either spacy or stanza."
		print(msg)
		raise argparse.ArgumentError(msg)
	return a

def format_output(o):
	if o not in ["xml", "pickle", "json"]:
		msg = "Output format " + o + " unknown. Output format must be either xml, pickle or json."
		print(msg)
		raise argparse.ArgumentError(msg)
	return o

def path_to_date(chemin):
	annee = chemin.split('/')[2]
	mois = chemin.split('/')[3]
	mois_num = str(liste_mois.index(mois) + 1)
	if len(mois_num) == 1:
		mois_num = '0' + mois_num
	jour = chemin.split('/')[4]
	date_str = annee + '-' + mois_num + '-' + jour

	date = format_date(date_str)
	return date

def path_to_categorie(fichier):
	categorie = fichier.split('/')[-1].split('.')[0][:-4] # retire .xml
	return categorie

def main():
	"""
	test des fonctions
	"""
	print(path_to_date('../corpus/2022/Apr/30/19-00-00/env_sciences.xml'))

#
# Main function
#

if __name__ == "__main__":
	main()
