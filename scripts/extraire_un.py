#!/bin/python3
#-*- coding: utf-8 -*-

#
# Modules to import
#
import argparse
import xml.etree.ElementTree as ET
import re
import sys
from pathlib import Path
import re

from datastructures import Article, Token
from data_format import path_to_date, path_to_categorie
from analyze import analyze
import feedparser



"""
Script qui contient trois fonctions différentes qui réalisent la même tâche:
Extraire les titres, les descriptions, la date et la catégorie d'un article rss.

Il est possible de choisir la méthode d'extraction (en donnant au script principal un argument -e).

"""

#
# User functions
#

def extraire(fichier_rss, extraction_method, analyse_method):
	if extraction_method == "et":
		return extraire_et(fichier_rss, analyse_method)
	elif extraction_method == "re":
		return extraire_re(fichier_rss, analyse_method)
	elif extraction_method == "fp":
		return extraire_fp(fichier_rss, analyse_method)
	else:
		msg = "Extraction method " + e + " unknown. It should be either et (ElementTree),	 re (Regular Expression) or fp (feedparser)."
		raise argparse.ArgumentError(msg)

# Module etree
def extraire_et(fichier_rss, analyse_method):
	text = Path(fichier_rss).read_text()
	if len(text) > 0:
		date = path_to_date(fichier_rss)
		categorie = path_to_categorie(fichier_rss)
		xml = ET.parse(fichier_rss)
		root = xml.getroot()
		for item in root.findall(".//item"):
			title = item.find("title").text
			desc = item.find("description").text
			if desc == None:
				continue
			analyse = list(analyze(desc, analyse_method))
			yield Article(title, desc, date, categorie, fichier_rss, analyse)


# expression régulière
def extraire_re(fichier_rss, analyse_method):
	text = Path(fichier_rss).read_text()
	if len(text) > 0:
		date = path_to_date(fichier_rss)
		categorie = path_to_categorie(fichier_rss)
		items = re.findall(r"<item>.*?</item>", text)  # list
		for item in items:
			title = re.findall(r"<title>.*?</title>", item)
			title = title[0].replace(
				"<title><![CDATA[", "").replace("]]></title>", "")
			# desc
			desc = re.findall(r"<description>.*?</description>", item)
			desc = desc[0].replace("<description><![CDATA[", "").replace("]]></description>", "")
			if desc == None:
				continue
			analyse = list(analyze(desc, analyse_method))
			yield Article(title, desc, date, categorie, fichier_rss, analyse)


# Module feedparser
def extraire_fp(fichier_rss, analyse_method):
    feed = feedparser.parse(fichier_rss)
    for entry in feed.entries:
        date = path_to_date(fichier_rss)
        categorie = path_to_categorie(fichier_rss)
        title = entry.title
        desc = entry.description
        analyse = list(analyze(desc, analyse_method))
        yield Article(title, desc, date, categorie, fichier_rss, analyse)


def main(args):
	"""
	Main pour tester les fonctions"

	"""
	for article in extraire_et(args.fichier):
		print(article.titre)
		print(article.description)

#
# Main function
#
if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('-f', '--fichier', type=str)
	args = parser.parse_args()
	main(args)
