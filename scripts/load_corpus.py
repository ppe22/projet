#!/bin/python3
# -*- coding: utf-8 -*-

"""
Prend en argument le nom du fichier du sous-corpus et le transforme en objets

"""
import xml.etree.ElementTree as ET
from pathlib import Path
import argparse
import pickle
import datetime
import json


from datastructures import Article, Corpus, Token, POS
from data_format import format_output, format_date

def fichier_to_corpus(fichier, format_in: format_output):
	if format_in == 'xml':
		return xml_to_corpus(fichier)
	elif format_in == 'json':
		return json_to_corpus(fichier)
	elif format_in == 'pickle':
		return pickle_to_corpus(fichier)

def xml_to_corpus(fichier: Path):
	text = Path(fichier).read_text()
	if len(text) > 0:
		xml = ET.parse(fichier)
		root = xml.getroot()
		categories = root.findall('./categorie')
		categories = [c.text for c in categories]
		begin = format_date(root.find('./begin').text)
		end = format_date(root.find('./end').text)
		chemin = root.find('./chemin').text
		
		corpus = Corpus(categories, begin, end, chemin, [])
		for article in root.findall('./article'):
			titre = article.find('titre').text
			description = article.find('description').text
			date = article.find('date').text
			categorie = article.find('categorie').text
			chemin = article.find('chemin').text
			analyse = article.find('analyse')
			article = Article(titre, description, date, categorie, chemin, [])
			for token in analyse.findall('token'):
				form = token.find('form').text
				lemma = token.find('lemma').text
				pos = POS(token.find('POS').text)
				article.analyse.append(Token(form, lemma, pos))
			corpus.articles.append(article)
		return corpus


def pickle_to_corpus(fichier: Path) -> Corpus:
    with open(fichier, "rb") as f:
        data = pickle.load(f)
        
    categories = data['categories']
    begin = datetime.datetime.strptime(str(data['begin']), "%Y-%m-%d")
    end = datetime.datetime.strptime(str(data['end']), "%Y-%m-%d")
    chemin = Path(data['chemin'])
    
    articles = []
    for article in data['articles']:
        titre = article['titre']
        description = article['description']
        date = datetime.datetime.fromisoformat(article['date'])
        categorie = article['categorie']
        chemin_article = Path(article['chemin'])
        
        tokens = []
        for token_dict in article['analyse']:
            form = token_dict['form']
            lemma = token_dict['lemma']
            pos = POS(token_dict['pos'])
            token = Token(form=form, lemma=lemma, pos=pos)
            tokens.append(token)
        
        article_obj = Article(
            titre=titre,
            description=description,
            date=date,
            categorie=categorie,
            chemin=chemin_article,
            analyse=tokens
        )
        articles.append(article_obj)
        
    corpus = Corpus(
        categories=categories,
        begin=begin,
        end=end,
        chemin=chemin,
        articles=articles
    )
    
    return corpus

def json_to_corpus(fichier: Path):
# à tester : j'ai pas compris comment récupérer begin, end et chemin !!
	articles = Path(fichier).read_text()
	articles = json.loads(articles)
	# articles fichier json / dictionnaire
	if len(articles) > 0:
		begin = articles["begin"] 
		end = articles["end"]
		categories = articles["categories"]
		chemin = articles["path"]
		
		corpus = Corpus(categories, begin, end, chemin, [])
		for article in articles["articles"]:
			# parcourt liste articles
			titre = article["titre"]
			description = article["description"]
			date = article["date"]
			categorie = article["categorie"]
			chemin = article["chemin"]
			analyse = []
			art = Article(titre, description, date, categorie, chemin, analyse)

			for t in article["analyse"]:
				form = t["form"]
				lemma = t["lemma"]
				pos = t["pos"]
				token = Token(form, lemma, pos)
				art.analyse.append(token)
			corpus.articles.append(art)

		return corpus
	

def main(args):
	fichier_to_corpus(args.fichier, args.format)

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('-f', '--fichier', type=str)
	parser.add_argument('-F', '--format', type=format_output)
	args = parser.parse_args()
	main(args)
