#!/bin/python3

#
# Modules to import
#

import os
from typing import Union
import datetime
from pathlib import Path
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import SubElement
from xml.dom import minidom # pour rendre le xml joli
import pickle
from data_format import format_date
import json

from datastructures import Article, Corpus, Token, POS

"""
Script qui permet d'enregistrer le sous-corpus au format voulu: xml, pickle ou json.

Il est possible de choisir le format (en donnant au script principal un argument -F)
"""

#
# User functions
#

def output_corpus(corpus: Corpus, destination: Union[str, Path], format_sortie="xml"):
    destination = Path(destination)
    if format_sortie == "xml":
        corpus_to_xml(corpus, destination)
    elif format_sortie == "json":
        corpus_to_json(corpus, destination)
    elif format_sortie == "pickle":
        corpus_to_pickle(corpus, destination.with_suffix('.pickle'))
    else:
        raise ValueError("Format de sortie invalide. Veuillez choisir 'xml', 'json' ou 'pickle'.")

def corpus_to_xml(corpus: Corpus, destination: Path):
	root = ET.Element('corpus')
	begin = ET.SubElement(root, 'begin').text = str(corpus.begin)
	end = ET.SubElement(root, 'end').text = str(corpus.end)
	for categorie in corpus.categories:
		cat = ET.SubElement(root, 'categorie').text = categorie
	chemin = ET.SubElement(root, 'chemin').text = corpus.chemin
	for article in corpus.articles:
		art = ET.SubElement(root, 'article')
		titre = ET.SubElement(art, 'titre').text = article.titre
		description = ET.SubElement(art, 'description').text = article.description
		date = ET.SubElement(art, 'date').text = datetime.datetime.strftime(article.date, "%Y-%m-%d") # conversion de la date en string
		cat = ET.SubElement(art, 'categorie').text = article.categorie
		chemin = ET.SubElement(art, 'chemin').text = article.chemin
		analyse = ET.SubElement(art, 'analyse')
		for token in article.analyse:
			tok = ET.SubElement(analyse, 'token')
			form = ET.SubElement(tok, 'form').text = token.form
			lemma = ET.SubElement(tok, 'lemma').text = token.lemma
			pos = ET.SubElement(tok, 'POS').text = token.pos.value

	tree = ET.tostring(root)
	xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="	")

	with open(destination, 'w') as f:
		f.write(xmlstr)

def corpus_to_pickle(corpus: Corpus, destination: Path):
    corpus_dict = {"articles": [], "from": corpus.begin, "to": corpus.end, "categories": corpus.categories, "destination": str(destination)}
    for article in corpus.articles:
        art = {"titre": article.titre, "description": article.description, "date": article.date.strftime("%Y-%m-%d"), "categorie": article.categorie, "chemin": article.chemin, "analyse": []}
        for token in article.analyse:
            tok = {"form": token.form, "lemma": token.lemma, "pos": token.pos.value}
            art["analyse"].append(tok)
        corpus_dict["articles"].append(art)
    corpus_dict["begin"] = corpus.begin
    corpus_dict["end"] = corpus.end
    corpus_dict["chemin"] = corpus.chemin # Add the 'begin' key-value pair
    with open(destination, "wb") as f:
        pickle.dump(corpus_dict, f)


def corpus_to_json(corpus: Corpus, destination: Path):
	json_object = json.dumps(json_output(corpus), indent=4)
	with open(destination, "w") as f:
		f.write(json_object)

def json_output(corpus: Corpus):
	# TODO Léna: Il faut adapter ta fonction pour qu'elle transforme un Corpus en dictionnaire avec toutes les informations qu'il faut dedans
	# dictionnaire {from: date, to: date, ..., articles: [...]}
	# articles est une liste de dictionnaire
	d = {}
	d["categories"] = corpus.categories
	d["begin"] = str(corpus.begin)
	d["end"] = str(corpus.end)
	d["path"] = corpus.chemin
	d["articles"] = []
	for article in corpus.articles:
		a = {}
		a["titre"] = article.titre
		a["description"] = article.description
		a["date"] = str(article.date)
		a["categorie"] = article.categorie
		a["chemin"] = article.chemin
		a["analyse"] = []
		for token in article.analyse:
			t = {}
			t["form"] = token.form
			t["lemma"] = token.lemma
			t["pos"] = token.pos.value
			a["analyse"].append(t)
		d["articles"].append(a)
	# Normalement, tu dois tout récupérer dans la variable corpus. Pas besoin d'aller rechercher la date etc dans le nom du fichier, tout est stocké dans la variable corpus.
	return d

def main():
	"""
	Fonctions de test
	"""
	corpus = xml_to_corpus("sous_corpus.pickle")
	print(corpus.begin, corpus.end)
	print(corpus.articles[0].analyse[0].lemma)

if __name__ == "__main__":
	main()
